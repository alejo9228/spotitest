//
//  ApiConstants.swift
//  SpotiTest
//
//  Created by MacBook Pro on 12/18/19.
//  Copyright © 2019 Condor Labs. All rights reserved.
//

import UIKit

struct ApiConstants {
    static let AuthorizationToken = "Bearer BQC1xr7A9jJpQB3pbfiMlXzNsAzWqACdAxonSUjHRuuRXhjEZIA91WAVJnB7zFqEc4EE6vZdTvD06HrxcnKi-1gaAF9cPxG9Ocbzxq4G3_1TUln2gPthhQeif_Ew3ZNv7glYl72CLrCKSisBUaRzyZPNrgDn9R8PBLU"
    static let ContentType = "application/json"
    static let BaseUrl = "https://api.spotify.com/v1/"
    static let SearchEndPoint = "search"
    static let AlbumsByArtistEndPoint = "artists/"
    
}

