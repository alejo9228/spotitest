//
//  ArtistRepository.swift
//  SpotiTest
//
//  Created by MacBook Pro on 12/18/19.
//  Copyright © 2019 Condor Labs. All rights reserved.
//

import UIKit
import Alamofire

class ArtistRepository {
    func FindArtist(name:String?, completion: @escaping (SearchArtistResponse?) -> Void){
        let headers: HTTPHeaders = [
            "Authorization": ApiConstants.AuthorizationToken,
            "Content-Type": ApiConstants.ContentType,
            "Accept": ApiConstants.ContentType
        ]
        
        let query = ApiConstants.BaseUrl + ApiConstants.SearchEndPoint + "?q="+(name?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed))!+"&type=artist"
        Alamofire.request(query, headers: headers).response { response in
            guard let data = response.data else { return }
            do {
                let decoder = JSONDecoder()
                let searchResponse = try decoder.decode(SearchArtistResponse.self, from: data)
                completion(searchResponse)
            } catch let error {
                print(error)
                completion(nil)
            }
        }
    }
    
    func FetchAlbumsByArtistId(id:String?, completion: @escaping (AlbumsResponse?) -> Void){
        let headers: HTTPHeaders = [
            "Authorization": ApiConstants.AuthorizationToken,
            "Content-Type": ApiConstants.ContentType,
            "Accept": ApiConstants.ContentType
        ]
        
        let query = ApiConstants.BaseUrl + ApiConstants.AlbumsByArtistEndPoint + id! + "/albums"
        Alamofire.request(query, headers: headers).response { response in
            guard let data = response.data else { return }
            do {
                let decoder = JSONDecoder()
                let albumResponse = try decoder.decode(AlbumsResponse.self, from: data)
                completion(albumResponse)
            } catch let error {
                print(error)
                completion(nil)
            }
        }
    }
}
