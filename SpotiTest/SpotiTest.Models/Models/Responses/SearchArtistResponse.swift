//
//  SearchArtistResponse.swift
//  SpotiTest
//
//  Created by MacBook Pro on 12/18/19.
//  Copyright © 2019 Condor Labs. All rights reserved.
//

import UIKit

struct SearchArtistResponse: Codable {
    let artists: Artists
}

// MARK: - Artists
struct Artists: Codable {
    let href: String
    let items: [Item]
    let limit: Int
    let offset: Int
    let total: Int
}

// MARK: - Item
struct Item: Codable {
    let followers: Followers
    let id: String
    let images: [Image]
    let name: String
    let popularity: Int

    enum CodingKeys: String, CodingKey {
        case followers, id, images, name, popularity
    }
}

// MARK: - Followers
struct Followers: Codable {
    let total: Int
}

// MARK: - Image
struct Image: Codable {
    let url: String
}
