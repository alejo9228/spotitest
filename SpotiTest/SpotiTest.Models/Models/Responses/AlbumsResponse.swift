//
//  AlbumsResponse.swift
//  SpotiTest
//
//  Created by MacBook Pro on 12/18/19.
//  Copyright © 2019 Condor Labs. All rights reserved.
//

import UIKit

struct AlbumsResponse: Codable {
    let href: String
    let items: [AlbumItem]
    let limit: Int
    let offset: Int
    let total: Int
}

// MARK: - Item
struct AlbumItem: Codable {
    let availableMarkets: [String]
    let externalUrls: ExternalUrls
    let href: String
    let id: String
    let images: [AlbumImage]
    let name: String

    enum CodingKeys: String, CodingKey {
        case availableMarkets = "available_markets"
        case externalUrls = "external_urls"
        case href, id, images, name
    }
}

// MARK: - Image
struct AlbumImage: Codable {
    let url: String
}

struct ExternalUrls: Codable {
    let spotify: String
}
