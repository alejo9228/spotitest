//
//  SearchViewPresenter.swift
//  SpotiTest
//
//  Created by MacBook Pro on 12/18/19.
//  Copyright © 2019 Condor Labs. All rights reserved.
//

import UIKit

protocol SearchViewDelegate {
    func UpdateList(artists:[Item]?, albums:[AlbumItem]?)
    func ShowAlbumDetail(album:AlbumItem?)
}

class SearchViewPresenter: NSObject {
    var delegate: SearchViewDelegate!
    let repository: ArtistRepository = ArtistRepository()
    
    func Search(input:String?) {
        var artists:[Item]?
        var albums:[AlbumItem]?
        
        if(input == "") {
            self.delegate.UpdateList(artists: nil, albums: nil)
            return
        }
        
        repository.FindArtist(name: input) { (searchResponse) in
            artists = searchResponse!.artists.items
            
            if(artists!.count > 0)
            {
                self.repository.FetchAlbumsByArtistId(id: artists?.first?.id) { (albumsResponse) in
                    albums = albumsResponse?.items
                    self.delegate.UpdateList(artists: artists, albums: albums)
                }
            }
        }
    }
    
    func SelectAlbum(album: AlbumItem?)
    {
        if(album != nil){
            self.delegate.ShowAlbumDetail(album: album)
        }
    }
}
