//
//  BaseViewController.swift
//  SpotiTest
//
//  Created by MacBook Pro on 12/17/19.
//  Copyright © 2019 Condor Labs. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    let operationQueue: OperationQueue = OperationQueue.main
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.black
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.darkGray
        navigationController?.navigationBar.tintColor = UIColor.white;
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        view.layoutIfNeeded()
        view.setNeedsLayout()
        view.autoresizingMask = UIView.AutoresizingMask(rawValue: UIView.AutoresizingMask.flexibleHeight.rawValue | UIView.AutoresizingMask.flexibleWidth.rawValue)
    }
}
