//
//  MainViewController.swift
//  SpotiTest
//
//  Created by MacBook Pro on 12/17/19.
//  Copyright © 2019 Condor Labs. All rights reserved.
//

import UIKit
import Alamofire

class SearchViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, SearchViewDelegate {
    var presenter: SearchViewPresenter!
    var coordinator: AlbumDetailCoordinator!
    var artists: [Item] = []
    var albums: [AlbumItem] = []
    var tableView: UITableView?
    var searchBar: UISearchBar?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = SearchViewPresenter()
        presenter.delegate = self
        
        title = "Search"
        
        InitializeComponents()
        SetViews()
        SetConstraints()
    }
    
    func InitializeComponents() {
        tableView = UITableView()
        tableView?.delegate = self
        tableView?.dataSource = self
        tableView?.rowHeight = UITableView.automaticDimension
        tableView?.translatesAutoresizingMaskIntoConstraints = false
        tableView?.register(ArtistTableViewCell.self, forCellReuseIdentifier: ArtistTableViewCell.Key)
        
        searchBar = UISearchBar()
        searchBar?.delegate = self
        searchBar?.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func SetViews() {
        searchBar?.barTintColor = UIColor.black
        searchBar?.tintColor = UIColor.white
        searchBar?.barStyle = .blackTranslucent
        searchBar?.placeholder = "Type an artist"
        
        tableView?.backgroundColor = UIColor.clear
        tableView?.separatorStyle = .none
        
        view.addSubview(searchBar!)
        view.addSubview(tableView!)
    }
    
    func SetConstraints() {
        let constraints: [NSLayoutConstraint] = [
            (searchBar?.topAnchor.constraint(equalTo: view.topAnchor, constant: 0))!,
            (searchBar?.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0))!,
            (searchBar?.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0))!,
            (tableView?.topAnchor.constraint(equalTo: searchBar!.bottomAnchor, constant: 0))!,
            (tableView?.widthAnchor.constraint(equalToConstant: view.frame.width))!,
            (tableView?.bottomAnchor.constraint(equalTo: view!.bottomAnchor, constant: 0))!
        ]
        
        NSLayoutConstraint.activate(constraints)
        
        view.layoutIfNeeded()
    }
    
    // SearchViewDelegate Overrides
    func UpdateList(artists: [Item]?, albums: [AlbumItem]?) {
        self.artists = artists == nil ? [] : artists!
        self.albums = albums == nil ? [] : albums!
        self.tableView?.reloadData()
    }
    
    func ShowAlbumDetail(album: AlbumItem?)
    {
        coordinator = AlbumDetailCoordinator(currentController: self, selectedAlbum: album!)
        coordinator.start()
    }
    
    func AlbumSelected(album: AlbumItem?) -> Void {
        self.presenter.SelectAlbum(album: album)
    }
    
    // UITableView Overrides
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return artists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let artist = artists[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: ArtistTableViewCell.Key) as! ArtistTableViewCell
        
        cell.SetData(artist: artist, index: indexPath.row, albums: albums, completition: AlbumSelected)
        return cell
    }
    
    // UISearchBar Overrides
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.presenter.Search(input: searchBar.text)
    }
}
