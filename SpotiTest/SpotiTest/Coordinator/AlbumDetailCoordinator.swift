//
//  AlbumDetailCoordinator.swift
//  SpotiTest
//
//  Created by MacBook Pro on 12/18/19.
//  Copyright © 2019 Condor Labs. All rights reserved.
//

import UIKit

class AlbumDetailCoordinator: Coordinator {
    let currentController: BaseViewController
    let selectedAlbum: AlbumItem
    
    var albumDetailView: AlbumDetailView!
  
    init(currentController: BaseViewController, selectedAlbum: AlbumItem) {
        self.currentController = currentController
        self.selectedAlbum = selectedAlbum
    }
  
    func start() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.HideRequested(_:)))
        
        albumDetailView = AlbumDetailView()
        albumDetailView.addGestureRecognizer(tap)
        albumDetailView.album = selectedAlbum
        
        UIApplication.shared.keyWindow!.addSubview(albumDetailView)
        
        let constraints: [NSLayoutConstraint] = [
            albumDetailView.topAnchor.constraint(equalTo: UIApplication.shared.keyWindow!.topAnchor),
            albumDetailView.leadingAnchor.constraint(equalTo: UIApplication.shared.keyWindow!.leadingAnchor),
            albumDetailView.trailingAnchor.constraint(equalTo: UIApplication.shared.keyWindow!.trailingAnchor),
            albumDetailView.bottomAnchor.constraint(equalTo: UIApplication.shared.keyWindow!.bottomAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
        
        UIApplication.shared.keyWindow!.layoutIfNeeded()
        
        Show()
    }
    
    @objc func HideRequested(_ sender: UITapGestureRecognizer? = nil) {
        Hide()
    }
    
    func Show(){
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
          self.albumDetailView.layer.opacity = 1
        }, completion: nil)
    }
    
    func Hide(){
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            self.albumDetailView.layer.opacity = 0
        }, completion: { finished in
            self.albumDetailView.removeFromSuperview()
        })
    }
}
