//
//  ApplicationCoordinator.swift
//  SpotiTest
//
//  Created by MacBook Pro on 12/18/19.
//  Copyright © 2019 Condor Labs. All rights reserved.
//

import UIKit

class ApplicationCoordinator: Coordinator {
  let window: UIWindow
  let rootViewController: UINavigationController
  
  init(window: UIWindow) {
    self.window = window
    
    let mainController = SearchViewController()
    rootViewController = UINavigationController(rootViewController: mainController)
  }
  
  func start() {
    window.rootViewController = rootViewController
    window.makeKeyAndVisible()
  }
}
