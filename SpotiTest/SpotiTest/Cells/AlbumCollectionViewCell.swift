//
//  AlbumCollectionViewCell.swift
//  SpotiTest
//
//  Created by MacBook Pro on 12/18/19.
//  Copyright © 2019 Condor Labs. All rights reserved.
//

import UIKit

class AlbumCollectionViewCell: UICollectionViewCell {
    static let Key = "AlbumCell"
    
    var photoImageView: UIImageView?
    var nameLabel: UILabel?
    var countryLabel: UILabel?
    
    var album: AlbumItem? {
        didSet{
            nameLabel?.text = self.album?.name
            if((self.album?.images.count)! > 0){
                photoImageView?.sd_setImage(with: URL(string: (self.album?.images[0].url)!))
            }
            
            var countryInfo:String = ""
            if(self.album!.availableMarkets.count < 5){
                for country in self.album!.availableMarkets
                {
                    countryInfo += country
                    countryInfo += ", "
                }
            }
            
            countryLabel?.text = countryInfo
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        InitializeComponents()
        SetViews()
        SetConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToSuperview() {
        SetLayers()
    }
    
    func InitializeComponents() {
        photoImageView = UIImageView()
        photoImageView?.translatesAutoresizingMaskIntoConstraints = false
        
        nameLabel = UILabel()
        nameLabel?.translatesAutoresizingMaskIntoConstraints = false
        
        countryLabel = UILabel()
        countryLabel?.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func SetViews() {
        photoImageView?.backgroundColor = UIColor.lightGray
        
        nameLabel?.font = .systemFont(ofSize: 16, weight: .bold)
        nameLabel?.textColor = .white
        
        countryLabel?.font = .systemFont(ofSize: 13)
        countryLabel?.textColor = .lightGray
        
        contentView.backgroundColor = UIColor.clear
        
        contentView.addSubview(photoImageView!)
        contentView.addSubview(nameLabel!)
        contentView.addSubview(countryLabel!)
    }
    
    func SetConstraints() {
        let constraints: [NSLayoutConstraint] = [
            (photoImageView?.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10))!,
            (photoImageView?.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0))!,
            (photoImageView?.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0))!,
            (photoImageView?.heightAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: CGFloat(0.4)))!,
            (nameLabel?.topAnchor.constraint(equalTo: photoImageView!.bottomAnchor, constant: 5))!,
            (nameLabel?.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0))!,
            (nameLabel?.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0))!,
            (countryLabel?.topAnchor.constraint(equalTo: nameLabel!.bottomAnchor, constant: 0))!,
            (countryLabel?.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0))!,
            (countryLabel?.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0))!,
            (countryLabel?.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: 0))!,
        ]
        
        contentView.bottomAnchor.constraint(equalTo: nameLabel!.bottomAnchor, constant: 10).isActive = true
        
        NSLayoutConstraint.activate(constraints)
        
        contentView.layoutIfNeeded()
    }
    
    func SetLayers() {
        photoImageView?.layer.cornerRadius = 10
        photoImageView?.layer.masksToBounds = true
    }
    
    func SetData(album: AlbumItem)
    {
        self.album = album
    }
}
