//
//  ArtistCellCollectionViewCell.swift
//  SpotiTest
//
//  Created by MacBook Pro on 12/17/19.
//  Copyright © 2019 Condor Labs. All rights reserved.
//

import UIKit
import SDWebImage

class ArtistTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate {
    static let Key = "SearchResultCell"
    
    var albumSelectedHandler: ((AlbumItem?) -> Void)?
    
    let photoSize = CGSize(width: 50, height: 50)
    
    var photoImageView: UIImageView?
    var titleLabel: UILabel?
    var followersLabel: UILabel?
    var popularityIndicator: UIButton?
    var collectionView: UICollectionView?
    
    var collectionViewHeightConstraint: NSLayoutConstraint!
    
    var artist : Item? {
        didSet {
            titleLabel?.text = self.artist?.name
            followersLabel?.text = "\(self.artist?.followers.total ?? 0)" + " FOLLOWERS"
            popularityIndicator?.setTitle("\(self.artist?.popularity ?? 0)" + " / 100", for: .normal)
            
            if((self.artist?.images.count)! > 0){
                photoImageView?.sd_setImage(with: URL(string: (self.artist?.images[0].url)!))
            }
        }
    }
    
    var albums : [AlbumItem]? {
        didSet {
            print("Albums count:")
            print(self.albums?.count as Any)
            collectionView?.reloadData()
        }
    }
    
    var isFirstRecord: Bool? {
        didSet{
            popularityIndicator?.isHidden = !self.isFirstRecord!
            collectionViewHeightConstraint.constant = self.isFirstRecord! ? 200 : 0
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        InitializeComponents()
        SetViews()
        SetConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToSuperview() {
        SetLayers()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return albums == nil ? 0 : albums!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let album = albums![indexPath.row]
        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: AlbumCollectionViewCell.Key, for: indexPath) as! AlbumCollectionViewCell
        if(cell == nil){
            let views = Bundle.main.loadNibNamed(AlbumCollectionViewCell.Key, owner: collectionView, options: nil)
            cell = views?.first as! AlbumCollectionViewCell
        }

        cell.SetData(album: album)
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let album = albums![indexPath.row]
        albumSelectedHandler!(album)
    }
    
    func InitializeComponents() {
        titleLabel = UILabel()
        titleLabel?.translatesAutoresizingMaskIntoConstraints = false
        
        photoImageView = UIImageView()
        photoImageView?.translatesAutoresizingMaskIntoConstraints = false
        
        followersLabel = UILabel()
        followersLabel?.translatesAutoresizingMaskIntoConstraints = false
        
        popularityIndicator = UIButton()
        popularityIndicator?.translatesAutoresizingMaskIntoConstraints = false
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: contentView.frame.width - 20 / 2, height: 200)
        layout.minimumLineSpacing = 10
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        layout.minimumInteritemSpacing = 10
        layout.scrollDirection = .horizontal
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout:layout)
        collectionView?.translatesAutoresizingMaskIntoConstraints = false
        collectionView?.register(AlbumCollectionViewCell.self, forCellWithReuseIdentifier: AlbumCollectionViewCell.Key)
        collectionView?.dataSource = self
        collectionView?.delegate = self
    }
    
    func SetViews() {
        self.selectionStyle = .none
        
        titleLabel?.textColor = UIColor.white
        titleLabel?.numberOfLines = 0
        titleLabel?.font = .systemFont(ofSize: 16, weight: .bold)
        
        followersLabel?.textColor = UIColor.white
        followersLabel?.numberOfLines = 1
        followersLabel?.font = .systemFont(ofSize: 13)
        
        photoImageView?.backgroundColor = UIColor.darkGray
        
        popularityIndicator?.backgroundColor = UIColor.green
        popularityIndicator?.contentHorizontalAlignment = .left
        popularityIndicator?.contentEdgeInsets = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
        popularityIndicator?.setTitleColor(UIColor.black, for: .normal)
        popularityIndicator?.titleLabel?.font = .systemFont(ofSize: 13, weight: .bold)
        
        collectionView?.backgroundColor = UIColor.clear
        
        contentView.backgroundColor = UIColor.clear
        backgroundColor = UIColor.clear
        
        contentView.addSubview(photoImageView!)
        contentView.addSubview(titleLabel!)
        contentView.addSubview(followersLabel!)
        contentView.addSubview(popularityIndicator!)
        contentView.addSubview(collectionView!)
    }
    
    func SetConstraints() {
        collectionViewHeightConstraint = collectionView?.heightAnchor.constraint(equalToConstant: 0)
        
        let constraints: [NSLayoutConstraint] = [
            (photoImageView?.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20))!,
            (photoImageView?.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5))!,
            (photoImageView?.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -10))!,
            (photoImageView?.widthAnchor.constraint(equalToConstant: photoSize.width))!,
            (photoImageView?.heightAnchor.constraint(equalToConstant: photoSize.height))!,
            (titleLabel?.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5))!,
            (titleLabel?.leadingAnchor.constraint(equalTo: photoImageView!.trailingAnchor, constant: 10))!,
            (titleLabel?.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20))!,
            (followersLabel?.leadingAnchor.constraint(equalTo: photoImageView!.trailingAnchor, constant: 10))!,
            (followersLabel?.topAnchor.constraint(equalTo: titleLabel!.bottomAnchor, constant: 5))!,
            (followersLabel?.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20))!,
            (popularityIndicator?.leadingAnchor.constraint(equalTo: photoImageView!.trailingAnchor, constant: 10))!,
            (popularityIndicator?.topAnchor.constraint(equalTo: followersLabel!.bottomAnchor, constant: 5))!,
            (popularityIndicator?.trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor, constant: -20))!,
            (collectionView?.topAnchor.constraint(equalTo: popularityIndicator!.bottomAnchor, constant: 10))!,
            (collectionView?.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0))!,
            (collectionView?.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0))!,
            (collectionView?.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10))!,
            collectionViewHeightConstraint
        ]
        
        NSLayoutConstraint.activate(constraints)
        
        contentView.layoutIfNeeded()
    }
    
    func SetData(artist: Item, index: Int, albums: [AlbumItem], completition: @escaping (AlbumItem?) -> Void) {
        self.artist = artist
        self.albums = albums
        self.isFirstRecord = index == 0
        self.albumSelectedHandler = completition
    }
    
    func SetLayers() {
        photoImageView?.layer.cornerRadius = photoSize.height/2
        photoImageView?.layer.masksToBounds = true
        
        popularityIndicator?.layer.cornerRadius = 5
        popularityIndicator?.layer.masksToBounds
    }
}
