//
//  AlbumDetailView.swift
//  SpotiTest
//
//  Created by MacBook Pro on 12/18/19.
//  Copyright © 2019 Condor Labs. All rights reserved.
//

import UIKit

class AlbumDetailView: UIView{
    var spotifyAlbumUrl:String = ""
    var album: AlbumItem? {
        didSet{
            if((self.album?.images.count)! > 0){
                photoImageView?.sd_setImage(with: URL(string: (self.album?.images[0].url)!))
            }
            spotifyAlbumUrl = (self.album?.externalUrls.spotify)!
        }
    }
    
    var shadowView: UIView?
    var photoImageView: UIImageView?
    var visitButton: UIButton?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        translatesAutoresizingMaskIntoConstraints = false
        
        InitializeComponents()
        SetViews()
        SetConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        SetLayers()
    }
    
    @objc func VisitButtonClicked(sender: UIButton!) {
        let url = URL(string: spotifyAlbumUrl)
        UIApplication.shared.open(url!)
    }
    
    func InitializeComponents(){
        shadowView = UIView()
        shadowView?.translatesAutoresizingMaskIntoConstraints = false
        
        photoImageView = UIImageView()
        photoImageView?.translatesAutoresizingMaskIntoConstraints = false
        
        visitButton = UIButton()
        visitButton?.translatesAutoresizingMaskIntoConstraints = false
        visitButton?.addTarget(self, action: #selector(VisitButtonClicked), for: .touchUpInside)
    }
    
    func SetViews(){
        layer.opacity = 0;
        backgroundColor = UIColor.black.withAlphaComponent(CGFloat(0.8))
    
        photoImageView?.backgroundColor = .blue
        
        visitButton?.setTitleColor(.black, for: .normal)
        visitButton?.backgroundColor = .green
        visitButton?.titleLabel?.font = .systemFont(ofSize: 16, weight: .bold)
        visitButton?.setTitle("CHECK IT OUT!", for: .normal)
        
        shadowView!.backgroundColor = UIColor.white.withAlphaComponent(CGFloat(0.6))
        shadowView!.addSubview(photoImageView!)
        shadowView!.addSubview(visitButton!)
        
        addSubview(shadowView!)
    }
    
    func SetConstraints(){
        let constraints: [NSLayoutConstraint] = [
            (photoImageView?.topAnchor.constraint(equalTo: shadowView!.topAnchor, constant: 40))!,
            (photoImageView?.leadingAnchor.constraint(equalTo: shadowView!.leadingAnchor, constant: 40))!,
            (photoImageView?.trailingAnchor.constraint(equalTo: shadowView!.trailingAnchor, constant: -40))!,
            (photoImageView?.heightAnchor.constraint(equalToConstant: 200))!,
            (photoImageView?.widthAnchor.constraint(equalToConstant: 200))!,
            (visitButton?.topAnchor.constraint(equalTo: photoImageView!.bottomAnchor, constant: 10))!,
            (visitButton?.leadingAnchor.constraint(equalTo: shadowView!.leadingAnchor, constant: 40))!,
            (visitButton?.trailingAnchor.constraint(equalTo: shadowView!.trailingAnchor, constant: -40))!,
            (visitButton?.bottomAnchor.constraint(equalTo: shadowView!.bottomAnchor, constant: -40))!,
            (visitButton?.heightAnchor.constraint(equalToConstant: CGFloat(30)))!,
            (shadowView?.centerYAnchor.constraint(equalTo: centerYAnchor))!,
            (shadowView?.centerXAnchor.constraint(equalTo: centerXAnchor))!,
        ]
        
        NSLayoutConstraint.activate(constraints)
        
        layoutIfNeeded()
    }
    
    func SetLayers(){
        shadowView?.layer.cornerRadius = 10
        shadowView?.layer.masksToBounds = true
        
        visitButton?.layer.cornerRadius = 15
        visitButton?.layer.masksToBounds = true
    }
}
